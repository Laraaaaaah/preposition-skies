const sky = document.getElementById("sky-color");
const windows = document.getElementsByClassName("window");
const buttons = document.getElementsByClassName("button");
const date = new Date();
const hour = date.getHours();
const root = document.querySelector(':root');


if (hour < 12) {
  root.style.setProperty('--sky-color', 'skyblue');
} else if (hour < 18) {
  root.style.setProperty('--sky-color', 'lightskyblue');
} else {
  root.style.setProperty('--sky-color', 'midnightblue');
}


function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function redirectToRandomPage() {
  const randomPageNumber = getRandomInt(1, 7);
  const randomFormat = Math.random() < 0.5 ? '' : '-R';
  const randomPageLink = `${randomPageNumber.toString().padStart(2, '0')}${randomFormat}.html`;
  window.location.href = randomPageLink;
}

const elseButtons = document.getElementsByClassName("else");
for (let i = 0; i < elseButtons.length; i++) {
  elseButtons[i].addEventListener("click", redirectToRandomPage);
}


function randomClouds() {
  const randomLeft = getRandomInt(1, 90);
  const randomTop = getRandomInt(1, 60);
  const cloud = document.getElementById("cloud-1");

  cloud.style.left = randomLeft + "vw";
  cloud.style.top = randomTop + "vh";
}

randomClouds()

function randomClouds2() {
  const randomLeft = getRandomInt(1, 90);
  const randomTop = getRandomInt(1, 60);
  const cloud = document.getElementById("cloud-2");

  cloud.style.left = randomLeft + "vw";
  cloud.style.top = randomTop + "vh";
}

randomClouds2()





